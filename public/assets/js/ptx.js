var ptx = {
    smsFormId: "#SmsCouponForm",
    modalId: "#FormModal",
    fieldPhoneNumber: "#PhoneNumber",
    fieldTAC: "#TermsAndConditions",
    fieldAgeConfirmation: "#AgeConfirmation",
    init: function () {
        ptx.formSumbitHandler();
    },
    formSumbitHandler: function () {
        jQuery(ptx.smsFormId).on('submit', function(event) {
            event.preventDefault();

            var correct = true;
            var phoneNumber = ptx.getCurrentPhoneNumber();
            if (ptx.validatePhoneNumber(phoneNumber) !== true) {
                correct = false;
                ptx.showErrorMessage(ptx.fieldPhoneNumber, 'error', 'You have entered invalid phone number');
            } else {
                ptx.showErrorMessage(ptx.fieldPhoneNumber, 'success', '');
            }

            var modalError = '';
            if (ptx.isTACChecked() !== true) {
                correct = false;
                modalError += 'You must accept Terms and conditions. ';
            }

            if (ptx.isAgeConfirmationChecked() !== true) {
                correct = false;
                modalError += 'You must confirm you are over 18 years old.';
            }

            if (modalError != '') {
                ptx.showModalError(modalError);
            }

            if (correct === true) {
                ptx.getCoupon4Phone(phoneNumber);
            }
        });
    },
    getCoupon4Phone: function (phoneNumber) {
        var msg = '<p>I am generating a coupon code especially for you. Please be patient.</p>';
        ptx.showModal(msg);

        jQuery.ajax({
            url : '/get-code',
            type : 'POST',
            dataType: 'json',
            data: {
                phone_number: phoneNumber
            }
        }).done(function(data) {
            var msg = '<p>Congratulation. Your prototion code is: <b>' + data.code + '</b>';
            ptx.showModal(msg);
        }).fail(function(data) {
            ptx.showErrorMessage('We are sorry, but we have encountered an internal issue. Please try to re-submit the form one more time.');
        });
    },
    showModalError: function (msg) {
        ptx.showModal('<p class="error">' + msg + '</p>');
    },
    showModal: function (msg) {
        var modal = jQuery(ptx.modalId);

        modal.find('.modal-body').html(msg);
        modal.modal('show');
    },
    showErrorMessage: function (fieldSelector, status, msg) {
        var element = jQuery(fieldSelector);
        var parentElement = element.parents('.form-group', 0);

        if (status == 'error') {
            parentElement.attr('class', 'form-group has-error');
            parentElement.find('.help-block').html(msg);
        } else {
            parentElement.attr('class', 'form-group');
            parentElement.find('.help-block').html(element.data('info'));
        }
    },
    validatePhoneNumber: function(phoneNumber) {
        var pattern = /^\+?[1-9]\d{1,14}$/;

        return pattern.test(phoneNumber);
    },
    getCurrentPhoneNumber: function() {
        return jQuery(ptx.fieldPhoneNumber).val();
    },
    isTACChecked: function() {
        return jQuery(ptx.fieldTAC + ':checked').length > 0;
    },
    isAgeConfirmationChecked: function() {
        return jQuery(ptx.fieldAgeConfirmation + ':checked').length > 0;
    },
    isBlank: function (string) {
        return (!str || /^\s*$/.test(str));
    }
};

$(document).ready(function() {
    ptx.init();
});
