# README #

The Front-end part to communicate with API application to provide support for promotions.

### Where to se it live? ###

* [Live version](https://ptxcoupons-frontend.herokuapp.com) - *it's a free heroku dyno, so it might take some time for it to start.*

### Possible settings? ###

I do not like to hard-code anything external, so the system uses environment variables for such cases. For the simplicity, you can change it under `/public/.htaccess` file, but I would prefer to set it up e.g. in heroku administration.