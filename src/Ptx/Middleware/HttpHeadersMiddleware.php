<?php declare(strict_types=1);

namespace Ptx\Middleware;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

class HttpHeadersMiddleware
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) : ResponseInterface {

        $response = $next($request, $response);

        return $response
                ->withAddedHeader('X-Frame-Options', 'deny')
                ->withAddedHeader('X-XSS-Protection', '1; mode=block')
                ->withAddedHeader('X-Content-Type-Options', 'nosniff');
    }
}
