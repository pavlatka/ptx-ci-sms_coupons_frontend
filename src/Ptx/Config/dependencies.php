<?php

$container['ApiComponent'] = function ($c) {
    $baseUrl   = getEnv('API_BASE_URL') ?: '';
    $authToken = getEnv('API_AUTH_TOKEN') ?: '';

    return new \Ptx\Component\ApiComponent($baseUrl, $authToken, $c['guzzle_client']);
};

$container['HomeController'] = function ($c) {
    return new \Ptx\Controller\HomeController($c['view']);
};

$container['GetPhoneController'] = function ($c) {
    return new \Ptx\Controller\GetPhoneController(
        $c['ApiComponent'],
        $c['view']
    );
};
