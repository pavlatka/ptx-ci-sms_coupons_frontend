<?php

use \Slim\Http\Request;
use \Slim\Http\Response;

$httpHeaders = new \Ptx\Middleware\HttpHeadersMiddleware();

$app->add($httpHeaders);

$app->get('/', 'HomeController:process');
$app->post('/get-code', 'GetPhoneController:process');
