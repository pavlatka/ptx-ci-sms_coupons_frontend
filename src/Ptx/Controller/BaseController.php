<?php declare(strict_types=1);

namespace Ptx\Controller;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

abstract class BaseController
{
    protected $args;
    protected $request;
    protected $response;
    protected $responseStatus = 200;
    protected $view;

    public function __construct(\Slim\Views\Twig $view)
    {
        $this->view = $view;
    }

    public function process(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $args
    ) : ResponseInterface {
        $this->args     = $args;
        $this->request  = $request;
        $this->response = $response;

        return $this->renderPage();
    }
}
