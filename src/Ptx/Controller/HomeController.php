<?php declare(strict_types=1);

namespace Ptx\Controller;

class HomeController extends BaseController
{
    public function renderPage()
    {
        return $this->view->render($this->response, 'homepage.html');
    }
}
