<?php declare(strict_types=1);

namespace Ptx\Controller;

use \Ptx\Component\ApiComponent;
use \Ptx\Component\ApiResponse;
use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

class GetPhoneController
{
    private $apiClient;

    public function __construct(
        \Ptx\Component\ApiComponent $apiClient
    ) {
        $this->apiClient = $apiClient;
    }

    public function process(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $args
    ) : ResponseInterface {

        $phoneNumber = $request->getParam('phone_number', null);
        $apiResponse = $this->apiClient->call(
            '/sms_promotions',
            array('phone_number' => $phoneNumber),
            'POST'
        );

        return $response->withJson($apiResponse->getBody(), $apiResponse->getResponseCode());
    }
}
