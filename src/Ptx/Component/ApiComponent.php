<?php declare(strict_types=1);

namespace Ptx\Component;

class ApiComponent
{
    private $baseUrl;
    private $authToken;
    private $httpClient;

    public function __construct(
        string $baseUrl,
        string $authToken,
        \GuzzleHttp\Client $httpClient
    ) {
        $this->baseUrl    = $baseUrl;
        $this->authToken  = $authToken;
        $this->httpClient = $httpClient;
    }

    public function call(string $url, array $data, string $method = 'POST') : ApiResponse
    {
        $response = null;
        $callUrl  = $this->getCallUrl($url);
        $method   = strtoupper($method);

        try {
            if ($method == 'POST') {
                $response = $this->httpClient->request(
                    $method,
                    $callUrl,
                    array(
                        'json' => $data,
                        'headers' => array(
                            'Authorization' => $this->authToken
                        )
                    )
                );

                $responseCode = $response->getStatusCode();
                $body         = json_decode($response->getBody()->getContents(), true);

                return $this->buildResponse($body, $responseCode);
            }
        } catch (\Exception $e) {
            return $this->buildResponse(array('message' => 'Internal Error'), 500);
        }
    }

    protected function buildResponse(array $body, int $responseCode) : ApiResponse
    {
        return new ApiResponse($body, $responseCode);
    }

    protected function getCallUrl(string $url) : string
    {
        return $this->baseUrl . $url;
    }
}
