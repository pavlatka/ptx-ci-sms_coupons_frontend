<?php declare(strict_types=1);

namespace Ptx\Component;

class ApiResponse
{
    private $body;
    private $responseCode;

    public function __construct(array $body, int $responseCode)
    {
        $this->body         = $body;
        $this->responseCode = $responseCode;
    }

    public function getBody() : array
    {
        return $this->body;
    }

    public function getResponseCode() : int
    {
        return $this->responseCode;
    }
}
