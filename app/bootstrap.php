<?php

require_once __DIR__ . '/../vendor/autoload.php';
$errorReporting = getEnv('ERROR_REPORTING') ?: 0;
$config = array(
    'settings' => array(
        'displayErrorDetails' => true
    )
);
$app = new \Slim\App($config);

$container = $app->getContainer();
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__  . '/tpls/');

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(
        new Slim\Views\TwigExtension($container['router'], $basePath)
    );

    return $view;
};

require_once __DIR__ . '/dependencies.php';
require_once __DIR__ . '/routes.php';

$app->run();
