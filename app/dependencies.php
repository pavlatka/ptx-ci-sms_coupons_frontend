<?php

$container = $app->getContainer();

$container['guzzle_client'] = function ($c) {
    return new \GuzzleHttp\Client();
};

require_once __DIR__ . '/../src/Ptx/Config/dependencies.php';
